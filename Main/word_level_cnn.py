import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, Dropout, Activation, Conv1D, concatenate, GlobalMaxPooling1D
from keras.models import Model
from keras import Sequential
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import pickle
import data_processing as p

max_features = 20000
maxlen = 35
embedding_file_txt = '../Data/glove.twitter.27B.200d.txt'
embedding_file_bin = '../Data/glove.twitter.27B.200d.txt.word2vec.bin'

(x_train, y_train), (x_val, y_val) = p.load_ag_data('../Data/train_en.tsv', '../Data/dev_en.tsv', hashtags=True, num=False, stopwords=True)
(x_train, y_train), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', hashtags=True, num=False, stopwords=True)
tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(x_train)
list_tokenized_train = tokenizer.texts_to_sequences(x_train)
X_t = pad_sequences(list_tokenized_train, maxlen=maxlen)
y = y_train
list_tokenized_val = tokenizer.texts_to_sequences(x_val)
x_val = pad_sequences(list_tokenized_val, maxlen=maxlen)
list_tokenized_test = tokenizer.texts_to_sequences(x_test)
x_test = pad_sequences(list_tokenized_test, maxlen=maxlen)

with open('../Data/tokenizer-word-cnn.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

wvs = p.WVsHandler(embedding_file_bin)
modelwords, vocabwords, dimwords = wvs.load()
embedding_layer = p.make_embedding_layer(tokenizer, dimwords, vocabwords, modelwords, maxlen)

word_inputs = Input(shape=(maxlen, dimwords, ), dtype='float32', name='word_inputs')
w_conv_1 = Conv1D(filters=64, kernel_size=1, use_bias=1, name='word_conv_1', padding='same')(word_inputs)
w_conv_2 = Conv1D(filters=64, kernel_size=2, use_bias=1, name='word_conv_2', padding='same')(word_inputs)
w_conv_3 = Conv1D(filters=64, kernel_size=3, use_bias=1, name='word_conv_3', padding='same')(word_inputs)
w_conv = concatenate([w_conv_1, w_conv_2, w_conv_3])
w_pool = GlobalMaxPooling1D()(w_conv)

nn = Model(input=word_inputs, output=w_pool)

model = Sequential()
model.add(embedding_layer)
model.add(Dropout(0.5))
model.add(nn)
model.add(Dense(2))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy',
              optimizer="sgd",
              metrics=['accuracy'])
print(model.summary())
filepath = "../Data/weights-best-word-temp.h5"
f1_ = 0.55
results = "none"
report_ = "none"
for x in range(0, 25):
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1,
                                 save_best_only=True, mode='max', save_weights_only=True)
    callbacks_list = [checkpoint]
    model.fit(X_t, y_train, batch_size=512, epochs=10, validation_data=(x_val, y_val), callbacks=callbacks_list)

    pred = model.predict_classes(x_test)

    gold = np.argmax(y_test, axis=1)
    report = classification_report(gold, pred)
    prec_results = precision(gold, pred, average='macro')
    rec_results = recall(gold, pred, average='macro')
    f1_results = f1(gold, pred, average='macro')
    accres = accuracy_score(gold, pred)
    if f1_results > f1_:
        f1_ = f1_results
        print(f1_)
        model.save('../Data/model-word-cnn.h5')
        model.save_weights(
            '../Data/weights-best-word-cnn.h5')
        report_ = report
        results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('===============')
print(report)
print(results)