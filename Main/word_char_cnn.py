import os

from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer

import pickle
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
import data_processing as p
import char_word_cnn_model as cwcm
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import numpy as np

dense_out = 560
maxlenc = 280
maxlenw = 35
vocab, reverse_vocab, vocab_size, alphabet = p.create_vocab_set()
categ_out = 2
embedding_file_bin = '../Data/glove.twitter.27B.200d.txt.word2vec.bin'
max_features = 20000

(x_train, y_train), (x_val, y_val) = p.load_ag_data('../Data/train_en.tsv', '../Data/dev_en.tsv', stem=True, hashtags=True)
x_train_c = p.encode_data(x_train, maxlenc, vocab)
x_val_c = p.encode_data(x_val, maxlenc, vocab)
(x_train_w, yt), (x_val_w, yv) = p.load_ag_data('../Data/train_en.tsv', '../Data/dev_en.tsv', stem=True, hashtags=True)

tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(x_train_w)
list_tokenized_train = tokenizer.texts_to_sequences(x_train)
x_train_w = pad_sequences(list_tokenized_train, maxlen=maxlenw)
list_tokenized_val = tokenizer.texts_to_sequences(x_val_w)
x_val_w = pad_sequences(list_tokenized_val, maxlen=maxlenw)

with open('../Data/tokenizer-word-char-cnn-r.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

wvs = p.WVsHandler(embedding_file_bin)
modelwords, vocabwords, dimwords = wvs.load()
embedding_layer = p.make_embedding_layer(tokenizer, dimwords, vocabwords, modelwords, maxlenw)

filepath = "../Data/weights-best-word-char-cnn-temp-r.h5"
f1_results = 0
f1_ = 0
results = "none"
report_ = "none"
for x in range(0, 1):
    model = cwcm.create_model(maxlenw, maxlenc, vocab_size, dense_out, categ_out, embedding_layer)

    print(model.summary())
    adam = Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    model.save("../Data/model-word-char-cnn-r.h5")
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1,
                                 save_best_only=True, mode='max', save_weights_only=True)
    callbacks_list = [checkpoint]
    model.fit([x_train_w, x_train_c], y_train, batch_size=80, epochs=10, shuffle=True,
              validation_data=([x_val_w, x_val_c], y_val),
              callbacks=callbacks_list)

    (unused, unused1), (x_test_c, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', stem=True, hashtags=True)
    (unused2, unused3), (x_test_w, unused4) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', stem=True, hashtags=True)
    x_test_c = p.encode_data(x_test_c, maxlenc, vocab)
    list_tokenized_test = tokenizer.texts_to_sequences(x_test_w)
    x_test_w = pad_sequences(list_tokenized_test, maxlen=maxlenw)

    y_predict = model.predict([x_test_w, x_test_c], batch_size=None, steps=None)

    y_predict = np.argmax(y_predict, axis=1)
    y_test = np.argmax(y_test, axis=1)

    gold = y_test
    report = classification_report(gold, y_predict)
    prec_results = precision(y_test, y_predict, average='macro')
    rec_results = recall(y_test, y_predict, average='macro')
    f1_results = f1(y_test, y_predict, average='macro')
    accres = accuracy_score(y_test, y_predict)
    if f1_results > f1_:
        f1_ = f1_results
        print(f1_)
        model.save('../Data/model-word-char-cnn-r.h5')
        model.save_weights('../Data/weights-best-word-char-cnn-r.h5')
        report_ = report
        results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('===============')
print(report)
print(results)
