from keras.models import Model
from keras.layers import Input, Dense, Dropout, Flatten, Lambda, Conv1D, concatenate, GlobalMaxPooling1D, Embedding, LSTM, Bidirectional

def create_model(maxlenw, maxlenc, vocab_size, dense_out, categ_out, embedding_layer):

    def one_hot(x):
        import tensorflow as tf
        return tf.one_hot(x, vocab_size, on_value=1.0, off_value=0.0, axis=-1, dtype=tf.float32)

    def one_hot_outshape(in_shape):
        return in_shape[0], in_shape[1], vocab_size

    word_inputs = Input(shape=(maxlenw, ), dtype='int64', name='word_inputs')
    embedded_w = embedding_layer(word_inputs)
    w_conv_1 = Conv1D(filters=128, kernel_size=1, use_bias=True, name='word_conv_1', padding='same')(embedded_w)
    w_conv_2 = Conv1D(filters=256, kernel_size=2, use_bias=True, name='word_conv_2', padding='same')(embedded_w)
    w_conv_3 = Conv1D(filters=512, kernel_size=3, use_bias=True, name='word_conv_3', padding='same')(embedded_w)
    w_conv = concatenate([w_conv_1, w_conv_2, w_conv_3])
    w_pool = GlobalMaxPooling1D()(w_conv)

    char_inputs = Input(shape=(maxlenc, ), dtype='int64', name='char_inputs')
    embedded_c = Lambda(one_hot, output_shape=one_hot_outshape)(char_inputs)
    c_conv_1 = Conv1D(filters=128, kernel_size=3, use_bias=True, name='char_conv_1', padding='same')(embedded_c)
    c_conv_2 = Conv1D(filters=256, kernel_size=4, use_bias=True, name='char_conv_2', padding='same')(embedded_c)
    c_conv_3 = Conv1D(filters=512, kernel_size=5, use_bias=True, name='char_conv_3', padding='same')(embedded_c)
    c_conv = concatenate([c_conv_1, c_conv_2, c_conv_3])
    c_pool = GlobalMaxPooling1D()(c_conv)

    merged_pool = concatenate([w_pool, c_pool])

    layers = [merged_pool]

    MAX = 2
    for i in range(MAX):
        current_layer = Dense(40, activation='relu', name='vanilla_nn_{}'.format(i), use_bias=True)(layers[-1])
        layers.append(current_layer)



    z = Dropout(0.5)(Dense(dense_out, activation='relu')(layers[-1]))

    z = Dropout(0.5)(Dense(dense_out, activation='relu')(z))

    pred = Dense(categ_out, activation='softmax', name='output', use_bias=True)(z)

    model = Model(inputs=[word_inputs, char_inputs], outputs=pred)
    return model