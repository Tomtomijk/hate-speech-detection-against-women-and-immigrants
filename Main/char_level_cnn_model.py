from keras.models import Model
from keras.layers import Input, Dense, Dropout, Lambda, Conv1D, concatenate, GlobalMaxPooling1D

def create_model(maxlen, vocab_size, dense_out, categ_out):

    def one_hot(x):
        import tensorflow as tf
        return tf.one_hot(x, vocab_size, on_value=1.0, off_value=0.0, axis=-1, dtype=tf.float32)

    def one_hot_outshape(in_shape):
        return in_shape[0], in_shape[1], vocab_size


    char_inputs = Input(shape=(maxlen, ), dtype='int64', name='char_inputs')
    embedded = Lambda(one_hot, output_shape=one_hot_outshape)(char_inputs)
    c_conv_1 = Conv1D(filters=64, kernel_size=3, use_bias=True, name='char_conv_1', padding='same')(embedded)
    c_conv_2 = Conv1D(filters=128, kernel_size=4, use_bias=True, name='char_conv_2', padding='same')(embedded)
    c_conv_3 = Conv1D(filters=256, kernel_size=5, use_bias=True, name='char_conv_3', padding='same')(embedded)
    c_conv = concatenate([c_conv_1, c_conv_2, c_conv_3])
    c_pool = GlobalMaxPooling1D()(c_conv)

    z = Dropout(0.5)(Dense(dense_out, activation='relu')(c_pool))
    z = Dropout(0.5)(Dense(dense_out, activation='relu')(z))

    pred = Dense(categ_out, activation='softmax', name='output', use_bias=True)(z)

    model = Model(inputs=char_inputs, outputs=pred)
    return model