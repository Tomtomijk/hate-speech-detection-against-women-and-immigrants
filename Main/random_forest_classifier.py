from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import data_processing as p

(x_train, y_train), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', categorical=False)

vectorizer = TfidfVectorizer()
x_vect_train = vectorizer.fit_transform(x_train)

clf = RandomForestClassifier(n_estimators=10)
model = clf.fit(x_vect_train, y_train)

x_vect_test = vectorizer.transform(x_test)

pred = model.predict(x_vect_test)
gold = y_test
report = classification_report(gold, pred, digits=3)
prec_results = precision(y_test, pred, average='macro')
rec_results = recall(y_test, pred, average='macro')
f1_results = f1(y_test, pred, average='macro')
accres = accuracy_score(y_test, pred)

results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('DATASET: Test')
print('===============')
print(report)
print(results)