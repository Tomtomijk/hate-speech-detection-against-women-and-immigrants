from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, LSTM, Dropout, Activation
from keras.layers import Bidirectional
from keras import Sequential
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import data_processing as p
import numpy as np
import pickle

max_features = 20000
maxlen = 35
embedding_file_txt = '../Data/glove.twitter.27B.200d.txt'
embedding_file_bin = '../Data/glove.twitter.27B.200d.txt.word2vec.bin'

(x_train, y_train), (x_val, y_val) = p.load_ag_data('../Data/train_en.tsv', '../Data/dev_en.tsv', stopwords=True, stem=True, hashtags=True)
(x_train, y_train), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', stopwords=True, stem=True, hashtags=True)

tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(x_train)
list_tokenized_train = tokenizer.texts_to_sequences(x_train)
X_t = pad_sequences(list_tokenized_train, maxlen=maxlen)
y = y_train
list_tokenized_val = tokenizer.texts_to_sequences(x_val)
x_val = pad_sequences(list_tokenized_val, maxlen=maxlen)
list_tokenized_test = tokenizer.texts_to_sequences(x_test)
x_test = pad_sequences(list_tokenized_test, maxlen=maxlen)

with open('../Data/tokenizer-word-lstm.pickle', 'wb') as handle:
    pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

wvs = p.WVsHandler(embedding_file_bin)
modelwords, vocabwords, dimwords = wvs.load()
embedding_layer = p.make_embedding_layer(tokenizer, dimwords, vocabwords, modelwords, maxlen)

model = Sequential()
model.add(embedding_layer)
model.add(Bidirectional(LSTM(64)))
model.add(Dense(2))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy',
              optimizer="sgd",
              metrics=['accuracy'])
print(model.summary())
filepath = "../Data/weights-best-word-lstm-temp.h5"
f1_ = 0.54
results = "none"
report_ = "none"
for x in range(0, 20):
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1,
                                 save_best_only=True, mode='max', save_weights_only=True)
    callbacks_list = [checkpoint]
    model.fit(X_t, y_train, batch_size=512, epochs=10, validation_data=(x_val, y_val), callbacks=callbacks_list)

    pred = model.predict_classes(x_test)

    gold = np.argmax(y_test, axis=1)
    report = classification_report(gold, pred)
    prec_results = precision(gold, pred, average='macro')
    rec_results = recall(gold, pred, average='macro')
    f1_results = f1(gold, pred, average='macro')
    accres = accuracy_score(gold, pred)
    if f1_results > f1_:
        f1_ = f1_results
        print(f1_)
        model.save('../Data/model-word-lstm.h5')
        model.save_weights(
            '../Data/weights-best-word-lstm.h5')
        report_ = report
        results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)


print('===============')
print(report_)
print(results)