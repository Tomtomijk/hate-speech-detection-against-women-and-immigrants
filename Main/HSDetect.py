import sys
from keras.engine.saving import load_model
import data_processing as p
import numpy as np, pandas as pd
from keras.preprocessing.sequence import pad_sequences
import pickle


def char_cnn():
    model = load_model('../Data/model-char-cnn.h5')
    model.load_weights('../Data/weights-best-char-cnn.h5')
    maxlen = 280
    vocab, reverse_vocab, vocab_size, alphabet = p.create_vocab_set()
    test = p.process_cl_input(pd.Series(sys.argv[1]))
    test = p.encode_data(test, maxlen, vocab)
    pred = model.predict(test)
    HS = 0
    if pred[0][0] <= 0.5 and pred[0][1] > 0.5:
        HS = 1
    print(sys.argv[1] + ' : ' + str(HS))


def word_cnn():
    model = load_model('../Data/model-word-cnn.h5')
    model.load_weights('../Data/weights-best-word-cnn.h5')
    maxlen = 35
    with open('../Data/tokenizer-word-char-cnn.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    test = p.process_cl_input(pd.Series(sys.argv[1]))
    list_tokenized_test = tokenizer.texts_to_sequences(test)
    test = pad_sequences(list_tokenized_test, maxlen=maxlen)
    pred = model.predict(test)
    HS = 0
    if pred[0][0] <= 0.5 and pred[0][1] > 0.5:
        HS = 1
    print(sys.argv[1] + ' : ' + str(HS))


def word_char_cnn():
    model = load_model('../Data/model-word-char-cnn.h5')
    model.load_weights('../Data/weights-best-word-char-cnn.h5')
    maxlenw = 35
    maxlenc = 280
    vocab, reverse_vocab, vocab_size, alphabet = p.create_vocab_set()
    with open('../Data/tokenizer-word-char-cnn.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)
    test = p.process_cl_input(pd.Series(sys.argv[1]), stem=True, hashtags=True)
    x_test_c = p.encode_data(test, maxlenc, vocab)
    list_tokenized_test = tokenizer.texts_to_sequences(test)
    x_test_w = pad_sequences(list_tokenized_test, maxlen=maxlenw)

    pred = model.predict([x_test_w, x_test_c])
    HS = 0
    if pred[0][0] <= 0.5 and pred[0][1] > 0.5:
        HS = 1
    print(sys.argv[1] + ' : ' + str(HS))



def word_lstm():
    model = load_model('../Data/model-word-lstm.h5')
    model.load_weights('../Data/weights-best-word-lstm.h5')
    maxlen = 35
    with open('../Data/tokenizer-word-lstm.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    test = p.process_cl_input(pd.Series(sys.argv[1]))
    list_tokenized_test = tokenizer.texts_to_sequences(test)
    test = pad_sequences(list_tokenized_test, maxlen=maxlen)
    pred = model.predict(test)
    HS = 0
    if pred[0][0] <= 0.5 and pred[0][1] > 0.5:
        HS = 1
    print(sys.argv[1] + ' : ' + str(HS))


def sgd_class():
    with open('../Data/sgd-class.pickle', 'rb') as handle:
        model = pickle.load(handle)
    test = p.process_cl_input(pd.Series(sys.argv[1]), stem=True, hashtags=True)
    pred = model.predict(test)
    print("\n" +sys.argv[1] + ' : ' + str(pred[0]) + "\n")


def tree_class():
    with open('../Data/tree-class.pickle', 'rb') as handle:
        model = pickle.load(handle)
    with open('../Data/tree-vectorizer.pickle', 'rb') as handle:
        vect = pickle.load(handle)
    test = vect.transform([sys.argv[1]])
    pred = model.predict(test)
    print(sys.argv[1] + ' : ' + str(pred[0]))


base = sys.argv[2]

if base == 'word-cnn':
    word_cnn()
elif base == 'word-char-cnn':
    word_char_cnn()
elif base == 'char-cnn':
    char_cnn()
elif base == 'sgd-class':
    sgd_class()
elif base == 'tree-class':
    tree_class()
elif base == 'word-lstm':
    word_lstm()
else:
    print("Unknown Model")
