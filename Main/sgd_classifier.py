from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
from sklearn.pipeline import Pipeline
import data_processing as p
import pickle

(x_train, y_train), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', categorical=False, hashtags=True, stopwords=True, num=True, stem=True)
pipeline_sgd = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf',  TfidfTransformer()),
    ('nb', SGDClassifier(max_iter=100, tol=100, shuffle=True)),
])

model = pipeline_sgd.fit(x_train, y_train)

with open('../Data/sgd-class.pickle', 'wb') as handle:
    pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)

pred = model.predict(x_test)
gold = y_test
report = classification_report(gold, pred)
prec_results = precision(y_test, pred, average='macro')
rec_results = recall(y_test, pred, average='macro')
f1_results = f1(y_test, pred, average='macro')
accres = accuracy_score(y_test, pred)

results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('===============')
print(report)
print(results)