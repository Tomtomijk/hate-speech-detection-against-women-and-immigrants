import sys
import os
import string
import re
import gensim
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import TweetTokenizer
from keras.utils.np_utils import to_categorical
from keras.layers import Embedding
from gensim.scripts.glove2word2vec import glove2word2vec
import numpy as np, pandas as pd

class WVsHandler:

    def __init__(self, embeddings_path):
        self.embeddings_path = embeddings_path

    def load(self):
        print('Loading embeddings:', self.embeddings_path)

        try:
            model = gensim.models.Word2Vec.load(self.embeddings_path)
        except:
            try:
                model = gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path)
            except:
                try:
                    model = gensim.models.KeyedVectors.load_word2vec_format(self.embeddings_path, binary=True)
                except:
                    sys.exit('Couldnt load embeddings')
        vocab = model.wv.index2word
        dims = model.__getitem__(vocab[0]).shape[0]
        vocab = set(vocab)
        return model, vocab, dims


def make_embedding_layer(tokenizer, vector_size, embedding_vocab, embedding_model, maxlen):
    word_index = tokenizer.word_index
    embedding_matrix = np.zeros((len(word_index) + 1, vector_size))
    for word, index in word_index.items():
        if word in embedding_vocab:
            embedding_matrix[word_index[word]] = embedding_model[word]
    embedding_layer = Embedding(len(word_index) + 1,
                                vector_size,
                                weights=[embedding_matrix],
                                input_length=maxlen,
                                trainable=False)
    return embedding_layer


def glove_to_word2vec(glove_file):
    if not os.path.isfile(glove_file + '.word2vec.bin'):
        word2vec_output_file = glove_file + '.word2vec'
        glove2word2vec(glove_file, word2vec_output_file)
        model = gensim.models.KeyedVectors.load_word2vec_format(word2vec_output_file)
        model.save_word2vec_format(word2vec_output_file + '.bin', binary=True)


def removeStopwords(tokens):
    stops = set(stopwords.words("english"))
    stops.update(['.', ',', '"', "'", '?', ':', ';', '(', ')', '[', ']', '{', '}'])
    toks = [tok for tok in tokens if not tok in stops and len(tok) >= 3]
    return toks


def removeURL(text):
    newText = re.sub('http\\S+', '', text, flags=re.MULTILINE)
    return newText


def removeNum(text):
    newText = re.sub('\\d+', '', text)
    return newText


def removeHashtags(tokens):
    toks = [tok for tok in tokens if tok[0] != '#']
    return toks


def modifyEmoji(text):
    eyes = r"[8:=;]"
    nose = r"['`\-]?"
    text = str(text)
    def re_sub(pattern, repl):
        return re.sub(pattern, repl, text)

    text = re_sub(r"{}{}[)dD]+|[)dD]+{}{}".format(eyes, nose, nose, eyes), "<smile>")
    text = re_sub(r"{}{}p+".format(eyes, nose), "<lolface>")
    text = re_sub(r"{}{}\(+|\)+{}{}".format(eyes, nose, nose, eyes), "<sadface>")
    text = re_sub(r"{}{}[\/|l*]".format(eyes, nose), "<neutralface>")
    text = re_sub(r"<3", "<heart>")
    return text


def stemTweet(tokens):
    stemmer = SnowballStemmer('english')
    stemmed_words = [stemmer.stem(word) for word in tokens]
    return stemmed_words


def processTweet(tweet, remove_swords=True, remove_url=True, remove_hashtags=True, remove_num=True, stem_tweet=True, modify_emoji=False):
    if remove_url:
        tweet = removeURL(tweet)
    if modify_emoji:
        tweet = modifyEmoji(tweet)
    twtk = TweetTokenizer(strip_handles=True, reduce_len=True)
    if remove_num:
        tweet = removeNum(tweet)
    tokens = [w.lower() for w in twtk.tokenize(tweet) if w != "" and w is not None]
    if remove_hashtags:
        tokens = removeHashtags(tokens)
    if remove_swords:
        tokens = removeStopwords(tokens)
    if stem_tweet:
        tokens = stemTweet(tokens)
    text = " ".join(tokens)
    return text

def create_vocab_set():
    alphabet = set(list(string.ascii_lowercase) + list(string.digits) +
                   list(string.punctuation) + ['\n'])
    vocab_size = len(alphabet)
    vocab = {}
    reverse_vocab = {}
    for ix, t in enumerate(alphabet):
        vocab[t] = ix
        reverse_vocab[ix] = t
    return vocab, reverse_vocab, vocab_size, alphabet


def encode_data(x, maxlen, vocab):
    input_data = np.zeros((len(x), maxlen), dtype=np.int)
    for dix, sent in enumerate(x):
        counter = 0
        for c in sent:
            if counter >= maxlen:
                pass
            else:
                ix = vocab.get(c, -1)
                input_data[dix, counter] = ix
                counter += 1
    return input_data


def process_cl_input(text, categorical=True, stopwords=False, url=True, hashtags=False, num=True, stem=False, emoji=False):
    test = text.map(lambda x: processTweet(x, remove_swords=stopwords, remove_url=url,
                                                       remove_hashtags=hashtags, remove_num=num, stem_tweet=stem, modify_emoji=emoji))
    test = np.array(test)
    return test


def load_ag_data(train, val, categorical=True, stopwords=False, url=True, hashtags=False, num=True, stem=False, emoji=False):
    train = pd.read_csv(train,
                        delimiter='\t', encoding='utf-8')
    train = train.dropna()
    x_train = train['text'].map(lambda x: processTweet(x, remove_swords=stopwords, remove_url=url,
                                                       remove_hashtags=hashtags, remove_num=num, stem_tweet=stem, modify_emoji=emoji))
    x_train = np.array(x_train)
    y_train = train['HS']
    if categorical:
        y_train = to_categorical(y_train)


    test = pd.read_csv(val, delimiter='\t',
                       encoding='utf-8')
    x_test = test['text'].map(lambda x: processTweet(x, remove_swords=stopwords, remove_url=url,
                                                       remove_hashtags=hashtags, remove_num=num, stem_tweet=stem, modify_emoji=emoji))
    x_test = np.array(x_test)

    y_test = test['HS']
    if categorical:
        y_test = to_categorical(y_test)

    return (x_train, y_train), (x_test, y_test)

filer_kernals = filter_kernels = [7, 7, 3, 3, 3, 3]
dense_outputs = 1024
maxlength = 512
vocab, reverse_vocab, vocab_size, alphabet = create_vocab_set()
nb_filter = 256
cat_output = 2
k = 40