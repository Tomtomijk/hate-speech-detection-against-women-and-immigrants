from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import data_processing as p
import pickle

(x_train, y_train), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv', categorical=False, hashtags=True, stopwords=True, emoji=True, stem=True)

vectorizer = TfidfVectorizer()
x_vect_train = vectorizer.fit_transform(x_train)

clf = DecisionTreeClassifier(max_depth=60)
model = clf.fit(x_vect_train, y_train)

# estimator = model # Doesn't work on desktop, works on colaboratory
# dot_data = export_graphviz(estimator, feature_names=vectorizer.get_feature_names(), filled=True, rounded=True, special_characters=True)
# graph = pydotplus.graph_from_dot_data(dot_data)
# display(Image(graph.create_png()))

x_vect_test = vectorizer.transform(x_test)

with open('../Data/tree-class.pickle', 'wb') as handle:
    pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open('../Data/tree-vectorizer.pickle', 'wb') as handle:
    pickle.dump(vectorizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

pred = model.predict(x_vect_test)
gold = y_test
report = classification_report(gold, pred, digits=3)
prec_results = precision(y_test, pred, average='macro')
rec_results = recall(y_test, pred, average='macro')
f1_results = f1(y_test, pred, average='macro')
accres = accuracy_score(y_test, pred)

results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('===============')
print(report)
print(results)