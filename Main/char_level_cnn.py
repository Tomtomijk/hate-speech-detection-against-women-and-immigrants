import os
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
import data_processing as p
import char_level_cnn_model as clcm
from sklearn.metrics import f1_score as f1, classification_report
from sklearn.metrics import precision_score as precision
from sklearn.metrics import recall_score as recall
from sklearn.metrics import accuracy_score
import numpy as np

dense_out = 560
maxlen = 280
vocab, reverse_vocab, vocab_size, alphabet = p.create_vocab_set()
filter_num = 140
categ_out = 2

(x_train, y_train), (x_val, y_val) = p.load_ag_data('../Data/train_en.tsv', '../Data/dev_en.tsv')
x_train = p.encode_data(x_train, maxlen, vocab)
x_val = p.encode_data(x_val, maxlen, vocab)
f1_ = 0.0
results = "none"
report_ = "none"
for x in range(0, 10):
    model = clcm.create_model(maxlen, vocab_size, dense_out, categ_out)

    print(model.summary())
    adam = Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    filepath = "../Data/weights-best-char-cnn-temp.h5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1,
                                 save_best_only=True, mode='max', save_weights_only=True)
    callbacks_list = [checkpoint]
    model.fit(x_train, y_train, batch_size=80, epochs=15, shuffle=True, validation_data=(x_val, y_val),
              callbacks=callbacks_list)

    (x_temp, y_temp), (x_test, y_test) = p.load_ag_data('../Data/train_en.tsv', '../Data/en.tsv')
    x_test = p.encode_data(x_test, maxlen, vocab)
    y_predict = model.predict(x_test, batch_size=None, steps=None)

    y_predict = np.argmax(y_predict, axis=1)
    y_test = np.argmax(y_test, axis=1)

    gold = y_test
    report = classification_report(gold, y_predict)
    prec_results = precision(y_test, y_predict, average='macro')
    rec_results = recall(y_test, y_predict, average='macro')
    f1_results = f1(y_test, y_predict, average='macro')
    accres = accuracy_score(y_test, y_predict)
    if f1_results > f1_:
        f1_ = f1_results
        print(f1_)
        model.save('../Data/model-char-cnn.h5')
        model.save_weights('../Data/weights-best-char-cnn.h5')
        report_ = report
        results = 'p=' + str(prec_results) + '\nr=' + str(rec_results) + '\nf=' + str(f1_results) + '\na=' + str(accres)

print('===============')
print(report)
print(results)
